# Este dockerfile es par tener como base de PHP. Los demás lo deben
# tomar como base

ARG VERSION=7.4
FROM php:${VERSION}

# Por las dudas, agrego nuevamente, ya que el "FROM" quita el argumento
ARG VERSION=7.4
ARG ICUVERSION=67
ARG MYSQLVERSION=mariadb
ARG PNGVERSION=16-16
ARG XDEBUG_VERSION=2.9.2

MAINTAINER   silvioq@gmail.com

RUN apt-get update && apt-get install -y  \
          curl \
          git  \
          libfreetype6-dev      \
          libjpeg62-turbo-dev   \
          libldap2-dev          \
          libicu-dev  \
          lib${MYSQLVERSION}-dev \
          libmcrypt-dev    \
          libmemcached-dev libmemcached11 \
          libpng-dev            \
          libpq-dev             \
          libssh2-1-dev         \
          libxml2-dev           \
          libxpm-dev            \
          libxslt-dev libxslt1.1 \
          locales               \
          zlib1g-dev            \
          libzip-dev            \
    && docker-php-ext-configure gd --with-jpeg \
    && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/  \
    && docker-php-ext-install -j$(nproc) bcmath gd iconv intl ldap mysqli opcache pdo_mysql pdo_pgsql soap sysvsem xsl zip \
    && pecl install xdebug-${XDEBUG_VERSION} \
    && pecl install memcached \
    && docker-php-ext-enable memcached \
    && apt-get remove -y libicu-dev libpq-dev libfreetype6-dev lib${MYSQLVERSION}-dev libpng-dev libjpeg62-turbo-dev zlib1g-dev libzip-dev libxslt-dev \
    && apt-get install -y libicu${ICUVERSION} libltdl7 libpq5 libfreetype6  libjpeg62-turbo lib${MYSQLVERSION}3 libmemcached-dev libmcrypt4 libpng${PNGVERSION}  libxpm4  libsasl2-2 zlib1g libzip4 \
    && apt-get autoremove -y  \
    && rm -rf /var/lib/apt/lists/* \
    && sed -i 's/# es_AR.UTF-8/es_AR.UTF-8/' /etc/locale.gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && php -v

COPY php.ini /usr/local/etc/php/php.ini

# Composer
ENV  COMPOSER_HOME=/opt/composer PATH=/opt/composer/vendor/bin:$PATH
RUN  curl  -sS  https://getcomposer.org/installer  | php -- --install-dir=/usr/local/bin --filename=composer --no-ansi \
  && composer -V

