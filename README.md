Imagen docker php 7 base para los proyectos
===========================================


Para construirla manualmente
----------------------------

```bash
git clone git@gitlab.com:silvioq/php-base-image.git
cd php-base-image
docker build -t php74 \
    --build-arg VERSION=7.4 \
    --build-arg ICUVERSION="67" \
    --build-arg PNGVERSION=16-16 \
    --build-arg MYSQLVERSION=mariadb  \
    --build-arg XDEBUG_VERSION=2.9.2
    .


# para construir php8.1
docker build -t php81 \
    --build-arg VERSION=8.1 \
    --build-arg ICUVERSION="72" \
    --build-arg PNGVERSION=16-16 \
    --build-arg MYSQLVERSION=mariadb  \
    --build-arg XDEBUG_VERSION=3.2.0

```
